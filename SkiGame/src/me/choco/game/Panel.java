package me.choco.game;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.swing.JPanel;
import javax.swing.Timer;

import me.choco.game.entities.Flag;
import me.choco.game.entities.Player;
import me.choco.game.entities.Tree;
import me.choco.game.utils.debug.ExceptionHandler;
import me.choco.game.utils.graphics.ImageLoader;
import me.choco.game.utils.graphics.SpriteSheet;

@SuppressWarnings("serial")
public class Panel extends JPanel implements Runnable, KeyListener{
	
	private Thread thread;
	private boolean running = false;
	
	Random random = new Random();
	
	private Graphics2D graphics;
	private BufferedImage image;
	private BufferedImage background;
	
	private int treesToAdd = 0;
	private int flagsToAdd = 0;
	private List<Tree> treesToRemove = new ArrayList<>();
	private List<Flag> flagsToRemove = new ArrayList<>();
	
	private List<Tree> trees = new ArrayList<>();
	private List<Flag> flags = new ArrayList<>();
	public Player player;
	
	private int timer = 100;
	
	public Panel(){
		setPreferredSize(new Dimension(SkiGame.WIDTH, SkiGame.HEIGHT));
		setFocusable(true);
		requestFocus();
	}
	
	public void addNotify(){
		super.addNotify();
		if (thread == null) {
			running = true;
			addKeyListener(this);
			thread = new Thread(this);
			thread.start();
			Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler());
		}
	}
	
	public void init(){
		image = new BufferedImage(SkiGame.WIDTH, SkiGame.HEIGHT, 1);
		graphics = ((Graphics2D) image.getGraphics());
		graphics.setFont(new Font("Arial", Font.BOLD, 20));
		background = ImageLoader.loadImage("/background.png");
		
		player = new Player(SkiGame.WIDTH / 2, 50, 30, 64, ImageLoader.loadImage("/skier_down.png"));
		
		ActionListener listener = new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e){
				for (Tree tree : trees)
					tree.setY(tree.getY() - 3);
				
				for (Flag flag : flags)
					flag.setY(flag.getY() - 3);
				
				if (random.nextInt(6) < 1 /* 17% */)
					treesToAdd++;
				
				if (random.nextInt(20) < 1 /* 5% */)
					flagsToAdd++;
				movementUpdates();
			}
		};
		new Timer(timer, listener).start();
	}
	
	public void update(){
		while (treesToAdd > 0){
			Tree tree = new Tree(random.nextInt(SkiGame.WIDTH), SkiGame.HEIGHT - 1, 42, 48, new SpriteSheet(ImageLoader.loadImage("/skier_tree.png")), 42, 48);
			tree.setSprite(random.nextInt(4));
			trees.add(tree);
			treesToAdd--;
		}
		
		while (flagsToAdd > 0){
			Flag flag = new Flag(random.nextInt(SkiGame.WIDTH), SkiGame.HEIGHT - 1, 12, 24, new SpriteSheet(ImageLoader.loadImage("/skier_flag.png")), 12, 24, random.nextInt(3) + 1);
			flag.setSprite(random.nextInt(2));
			flags.add(flag);
			flagsToAdd--;
		}
		
		trees.removeAll(treesToRemove);
		flags.removeAll(flagsToRemove);
	}
	
	public void draw(){
		graphics.clearRect(0, 0, SkiGame.WIDTH, SkiGame.HEIGHT);
		graphics.drawImage(background, 0, 0, SkiGame.WIDTH, SkiGame.HEIGHT, null);
		graphics.setColor(Color.YELLOW);
		
		player.render(graphics);
		
		for(Tree tree : trees){
			graphics.setColor(Color.GREEN);
			tree.render(graphics);
		}
		
		for (Flag flag : flags){
			graphics.setColor(Color.RED);
			flag.render(graphics);
		}
		
		graphics.setColor(Color.BLUE);
		graphics.drawString("Score: " + player.getScore(), 3, 20);
	}
	
	public void drawToScreen(){
		Graphics panelGraphics = getGraphics();
		panelGraphics.drawImage(image, 0, 0, SkiGame.WIDTH, SkiGame.HEIGHT, null);
		panelGraphics.dispose();
	}

	@Override
	public void run(){
		init();
		while (running){
			update();
			draw();
			drawToScreen();
		}
	}

	public void keyPressed(KeyEvent event) {
		if (!player.isDead()){
			switch(event.getKeyCode()){
			case KeyEvent.VK_UP:
				player.setY(player.getY() - 2);
				break;
			case KeyEvent.VK_LEFT:
				player.setX(player.getX() - 2);
				break;
			case KeyEvent.VK_DOWN:
				player.setY(player.getY() + 2);
				break;
			case KeyEvent.VK_RIGHT:
				player.setX(player.getX() + 2);
				break;
			default:
				break;
			}
			movementUpdates();
		}else{
			switch(event.getKeyCode()){
			case KeyEvent.VK_R:
				player.setCoordinates(SkiGame.WIDTH / 2, 50);
				player.setDead(false);
				break;
			default:
				break;
			}
		}
	}
	public void keyTyped(KeyEvent event) {}
	public void keyReleased(KeyEvent event) {}
	
	private void movementUpdates(){
		for (Tree tree : trees){
			if (player.isCollidingWith(tree)){
				if (player.isDead()) return;
				
				player.setLives(player.getLives() - 1);
				if (player.getLives() <= 0){
					player.setDead(true);
				}
			}
		}
		
		for (Flag flag : flags){
			if (player.isCollidingWith(flag)){
				if (player.isDead()) return;
				
				player.setScore(player.getScore() + flag.getValue());
				flagsToRemove.add(flag);
			}
		}
		
		if (player.isOutOfBounds()){
			player.setLives(player.getLives() - 1);
			if (player.getLives() <= 0){
				player.setDead(true);
			}
		}
		
		for (Tree tree : trees)
			if (tree.isOutOfBounds()) treesToRemove.add(tree);
		
		for (Flag flag : flags)
			if (flag.isOutOfBounds()) flagsToRemove.add(flag);
	}
}