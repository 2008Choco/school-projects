package me.choco.game.utils.graphics;

import java.awt.image.BufferedImage;
import java.util.ArrayList;

public class SpriteSheet{
	
	BufferedImage sheet;
	public SpriteSheet(BufferedImage sheet){
		this.sheet = sheet;
	}
	
	public BufferedImage getSubsprite(int x, int y, int width, int height){
		return sheet.getSubimage(x, y, width, height);
	}
	
	public ArrayList<BufferedImage> getAllSubsprites(int spriteWidth, int spriteHeight){
		ArrayList<BufferedImage> sprites = new ArrayList<>();
		int widthCount = sheet.getWidth() / spriteWidth;
		int heightCount = sheet.getHeight() / spriteHeight;
		
		for (int y = 0; y < heightCount; y++){
			for (int x = 0; x < widthCount; x++){
				sprites.add(getSubsprite(x * spriteWidth, y * spriteHeight, spriteWidth, spriteHeight));
			}
		}
		return sprites;
	}
}