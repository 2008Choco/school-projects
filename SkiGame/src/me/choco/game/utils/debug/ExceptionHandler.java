package me.choco.game.utils.debug;

import java.lang.Thread.UncaughtExceptionHandler;

public class ExceptionHandler implements UncaughtExceptionHandler{
	@Override
	public void uncaughtException(Thread t, Throwable e){
		if (Debug.FANCY_ERRORS){ Debug.printStackTrace(e, t); }
		else{ e.printStackTrace(); }
	}
}