package me.choco.game.utils.debug;

public class Debug{
	public static boolean SHOW_ENTITY_BOUNDS = false;
	public static boolean SHOW_DEBUG_INFO = false;
	public static boolean FANCY_ERRORS = true;
	
	public static void logInfo(String message){
		if (SHOW_DEBUG_INFO) System.out.println(message);
	}
	
	public static void printStackTrace(Throwable e, Thread t){
		if (SHOW_DEBUG_INFO){
			System.err.println("----------------------------------------------------");
			System.err.println("Error Detected: ");
			System.err.println(e.toString());
			System.err.println("----------------------------------------------------");
			System.err.println("Thread:");
			System.err.println("    Name: " + t.getName());
			System.err.println("    Id: " + t.getId());
			System.err.println("    Is Running: " + t.isAlive());
			System.err.println("----------------------------------------------------");
			System.err.println("Stacktrace:");
			e.printStackTrace();
			System.err.println("----------------------------------------------------");
		}
	}
}