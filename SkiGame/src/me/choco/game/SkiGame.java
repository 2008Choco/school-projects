package me.choco.game;

import java.awt.Dimension;
import java.awt.Toolkit;

import javax.swing.JFrame;

import me.choco.game.utils.graphics.ImageLoader;

public class SkiGame{
	
	public static JFrame frame = new JFrame("Ski Game");
	
	private static final Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
	public static int WIDTH = (int) Math.round(screenSize.getWidth() / 1.5);
	public static int HEIGHT = (int) Math.round((screenSize.getHeight() / 1.5) + screenSize.getHeight() / 4.5);
	
	public static void main(String[] args){
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(WIDTH, HEIGHT);
		frame.setContentPane(new Panel());
		frame.setResizable(false);
		frame.setLocationRelativeTo(null);
		frame.setIconImage(ImageLoader.loadImage("/skier_crash.png"));
		frame.setVisible(true);
	}
}