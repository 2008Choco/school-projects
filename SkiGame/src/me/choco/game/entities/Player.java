package me.choco.game.entities;

import java.awt.image.BufferedImage;

import me.choco.game.utils.graphics.ImageLoader;

public class Player extends Entity{
	
	private int lives = 1;
	private boolean dead = false;
	private int score = 0;

	public Player(int x, int y, int width, int height, BufferedImage sprite){
		super(x, y, width, height, sprite);
	}
	
	public int getLives() {
		return lives;
	}

	public void setLives(int lives) {
		this.lives = lives;
	}

	public boolean isDead() {
		return dead;
	}

	public void setDead(boolean dead) {
		this.dead = dead;
		if (dead) setSprite(ImageLoader.loadImage("/skier_crash.png"));
		else setSprite(ImageLoader.loadImage("/skier_down.png"));
	}
	
	public int getScore(){
		return score;
	}
	
	public void setScore(int score){
		this.score = score;
	}
}