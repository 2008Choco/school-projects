package me.choco.game.entities;

import java.awt.image.BufferedImage;

import me.choco.game.utils.graphics.SpriteSheet;

public class Tree extends Entity{
	public Tree(int x, int y, int width, int height, BufferedImage sprite) {
		super(x, y, width, height, sprite);
	}
	
	public Tree(int x, int y, int width, int height, SpriteSheet sprite, int spriteWidth, int spriteHeight) {
		super(x, y, width, height, sprite, spriteWidth, spriteHeight);
	}
}