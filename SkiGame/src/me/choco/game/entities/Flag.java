package me.choco.game.entities;

import java.awt.image.BufferedImage;

import me.choco.game.utils.graphics.SpriteSheet;

public class Flag extends Entity{
	
	protected int pointValue = 1;
	
	public Flag(int x, int y, int width, int height, BufferedImage sprite) {
		super(x, y, width, height, sprite);
	}
	
	public Flag(int x, int y, int width, int height, BufferedImage sprite, int pointValue) {
		super(x, y, width, height, sprite);
		this.pointValue = pointValue;
	}
	
	public Flag(int x, int y, int width, int height, SpriteSheet sprite, int spriteWidth, int spriteHeight) {
		super(x, y, width, height, sprite, spriteWidth, spriteHeight);
	}
	
	public Flag(int x, int y, int width, int height, SpriteSheet sprite, int spriteWidth, int spriteHeight, int pointValue) {
		super(x, y, width, height, sprite, spriteWidth, spriteHeight);
		this.pointValue = pointValue;
	}
	
	public int getValue(){
		return pointValue;
	}
	
	public void setValue(int value){
		this.pointValue = value;
	}
}