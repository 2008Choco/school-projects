package me.choco.game.entities;

import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

import me.choco.game.SkiGame;
import me.choco.game.utils.debug.Debug;
import me.choco.game.utils.graphics.SpriteSheet;

public abstract class Entity{
	
	int x; int y;
	int width; int height;
	BufferedImage sprite;
	ArrayList<BufferedImage> sprites = new ArrayList<>();
	public Entity(int x, int y, int width, int height, BufferedImage sprite){
		this.x = x; this.y = y;
		this.width = width; this.height = height;
		this.sprite = sprite;
	}
	
	public Entity(int x, int y, int width, int height, SpriteSheet spriteSheet, int spriteWidth, int spriteHeight){
		this.x = x; this.y = y;
		this.width = width; this.height = height;
		this.sprite = spriteSheet.getSubsprite(0, 0, spriteWidth, spriteHeight);
		this.sprites = spriteSheet.getAllSubsprites(spriteWidth, spriteHeight);
	}
	
	public int getX() {
		return x;
	}
	
	public void setX(int x) {
		this.x = x;
	}
	
	public int getY() {
		return y;
	}
	
	public void setY(int y) {
		this.y = y;
	}
	
	public void setCoordinates(int x, int y){
		this.x = x; this.y = y;
	}
	
	public int getWidth(){
		return width;
	}
	
	public void setWidth(int width){
		this.width = width;
	}
	
	public int getHeight(){
		return height;
	}
	
	public void setHeight(int height){
		this.height = height;
	}
	
	public BufferedImage getSprite(){
		return sprite;
	}
	
	public void setSprite(BufferedImage sprite){
		this.sprite = sprite;
		setWidth(sprite.getWidth());
		setHeight(sprite.getHeight());
	}
	
	public void setSprite(int index){
		if (!sprites.isEmpty()){
			this.sprite = sprites.get(index);
			setWidth(sprite.getWidth());
			setHeight(sprite.getHeight());
		}else{System.err.println("Entity ID " + this.toString() + " does not have a registered SpriteSheet");}
	}
	
	public Rectangle getBounds(){
		return new Rectangle(x, y, width, height);
	}
	
	public boolean isCollidingWith(Entity entity){
		if (entity.getBounds().intersects(this.getBounds()))
			return true;
		return false;
	}
	
	public boolean isOutOfBounds(){
		if (this.getX() <= (0 - this.getWidth()) || this.getX() > SkiGame.WIDTH 
				|| this.getY() <= (0 - this.getHeight()) || this.getY() > SkiGame.HEIGHT)
			return true;
		return false;
	}
	
	public void render(Graphics g){
		if (Debug.SHOW_ENTITY_BOUNDS) g.drawRect(x, y, width, height);
		g.drawImage(getSprite(), x, y, width, height, null);
	}
}