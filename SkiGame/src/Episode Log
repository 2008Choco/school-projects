Episode 1: Creating Our Game Window
- Added SkiGame.class (Containing main method)
- Added the generation of a default/empty JFrame

Episode 2: Commencing Graphics
- Added Panel.class (Containing all game-related code)
- Added functionality for Graphics2D

Episode 3: Entities and Players
- Added an abstract Entity class
- Added a Player class that extends Entity

Episode 4: Rendering and Drawing Players/Entities
- Added some code to the Entity.render() method
- Added a new Player instance and variable in the Panel.class
- Added width and height integer parameters to the Entity class (and subclasses)
- Added getWidth() and getHeight() methods to the abstract Entity class

Episode 5: Keyboard Input / Player Movement
- Added player movement using the arrow keys

Episode 6: So Many Entitrees!!!
- Added a Tree.class and a Flag.class
- Rendered a new tree and flag on the screen

Episode 7: Sprites and Images
- Added a new utils package in me.choco.game
- Added an ImageLoader class to the utils class
- Added a BufferedImage (sprite) parameter to the Entity, Player, Flag, and Tree classes
- Rendered sprites to all entities
- Modified the render method in the Entity class to draw images rather than boxes

Episode 8: Entity Collision
- Added getBounds() and isCollidingWith(Entity) methods in the Entity class
- Added private movementUpdates() method in the main Panel class to do updates upon moving
- Added basic entity collision

Episode 9: Deaths and Lives
- Added lives int field in the Player class
- Added dead boolean field in the Player class
- Added getters and setters for above fields
- Added a render check to see if the player is dead or not, then render depending on the boolean state

Episode 10: Time and Tree Spawns
- Added Timer (And ActionListener) to spawn trees at intervals of time
- Added randomly generating trees

Episode 11: Optimizations and Debugging!!
- Removed Entities when moving out of bounds
- Changed the tree methods from ".setCoordinates()" to ".setY()" as that is all we're moving

Episode 11.5: Debug Utiliy Class
- Fixed Trees going over the player when the player stands still
- Fixed Entities not truly being removed until the player moves
- Added a Debug.class, similar to a configuration file

Episode 12: Scoreboards and Scoring Systems
- Added a new protected integer in the Flag class, to specify the point value
- Added a new constructor to be able to optionally specify a point value
- Added movement, as well as random generation for flags

|---------------- =            =              =                =                = ----------------|
BETWEEN EPISODES: Added an Exception handler to print out exceptions in a neater, more readable way
|---------------- =            =              =                =                = ----------------|

Episode 12.5: Bug Fixing / Optimizations
- Fixed receiving points after death
- Fixed being able to lose a life after death
- Fixed a ConcurrentModificationException after x amount of time (Used the ArrayList.removeAll() method)

Episode 13: Polish - Randomized Trees and Flags
- Added a new SpriteSheet class
- Added new 'sprites' ArrayList<BufferedImage> variable in the Entity.class
- Added setter methods for sprites, width, and height in the Entity.class
- Added new constructors for the Entity.class, as well as all subclasses
- Added a Spritesheet for all Trees (4 types), as well as the flags (2 directions)

Episode 13.5: Finishing Up Texture Randomization & Fixes
- Fixed the getAllSprites() method in the SpriteSheet not functioning properly
- Added true randomization to Tree and Flag textures (:D They work)

|---------------- =               =              =                =                   = ----------------|
BETWEEN EPISODES: Allowed Trees / Flags to spawn relative to height of screen (Flexible for resizability)
                  Changed the size of the game to be relative to the size of the monitor
|---------------- =               =              =                =                   = ----------------|

Episode 14: Polish - The Background
- Added static background image
- Added a temporary icon to the game

Episode 15: Polish - Final Revising / Code Optimizations
- TODO:
    - Just look over all and explain the code
    - Optimize whatever possible

Episode 16: The End - Our Finished Game
- TODO:
    - The game is completely finished. Play a round or two on camera to see what I can get as a high score
    
Episode 17: BONUS - Events?
- TODO:
	- Create events (Those mentioned below in the TODO)


NOTE:
AFTER ALL THE TUTORIALS ARE COMPLETED, UPLOAD THEM THE YouTube AND PUT THEM AS UNLISTED. SEND THE VIDEO LINK TO MR. McDaniel TO SEND TO ALL 
FUTURE STUDENTS THAT MAY WANT TO LEARN HOW TO DEVELOP A GAME IN JAVA.

TODO:
- Create events
  - EntityInteractWithEntityEvent
  - PlayerMoveEvent
  - GameTickEvent