package me.choco.converter;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JFrame;

import me.choco.converter.utils.ConversionPanel;

public class B2DConverter {
	public static void main(String[] args) {
		BufferedImage image = null;
		try {image = ImageIO.read(new File("Resources/icon.png"));}catch (IOException e) {}
		
		JFrame frame = new JFrame("Binary-Decimal Converter");
		frame.setEnabled(true);
		frame.setIconImage(image);
		frame.setSize(325, 150);
		frame.setResizable(false);
		frame.setLocationRelativeTo(null);
		frame.add(new ConversionPanel());
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
	}
}