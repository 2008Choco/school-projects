package me.choco.converter.utils;

public class BinaryMath {
	public int binaryToDecimal(String input){
		int decimalOutput = 0;
		try{
			String[] inputAsArray = input.split("");
			for (int i = 0; i < inputAsArray.length; i++){
				decimalOutput += (Integer.parseInt(inputAsArray[i]) * Math.pow(2, i));
			}
		}catch(NumberFormatException e){}
		return decimalOutput;
	}
	
	public String decimalToBinary(String input){
		StringBuilder binaryOutput = new StringBuilder();
		try{
			int inputInt = Integer.parseInt(input);
			while(inputInt != 0){
				binaryOutput.append(String.valueOf(inputInt%2));
				inputInt /= 2;
			}
		}catch(NumberFormatException e){}
		return binaryOutput.toString();
	}
}