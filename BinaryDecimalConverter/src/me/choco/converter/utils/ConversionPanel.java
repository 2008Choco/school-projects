package me.choco.converter.utils;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

@SuppressWarnings("serial")
public class ConversionPanel extends JPanel{
	
	public JTextField binaryField = new JTextField(); public JLabel binaryLabel = new JLabel("Binary: ");
	public JTextField decimalField = new JTextField(); public JLabel decimalLabel = new JLabel("Decimal: ");
	public JButton convertButton = new JButton("Convert");
	
	private BinaryMath math = new BinaryMath();
	
	public ConversionPanel(){
		setLayout(new GridBagLayout());
		GridBagConstraints constraints = new GridBagConstraints();
		constraints.insets = new Insets(5, 5, 5, 5);
		constraints.anchor = GridBagConstraints.CENTER;
		constraints.fill = GridBagConstraints.BOTH;
		
		constraints.gridx = 0;
		constraints.gridy = 0;
		add(binaryLabel, constraints);
		
		constraints.gridx = 1;
		constraints.weightx = 0.5;
		add(binaryField, constraints);
		
		constraints.gridx = 1;
		constraints.gridy = 1;
		constraints.weightx = 0.0;
		add(convertButton, constraints);
		
		constraints.gridx = 0;
		constraints.gridy = 2;
		add(decimalLabel, constraints);
		
		constraints.gridx = 1;
		constraints.weightx = 0.5;
		add(decimalField, constraints);
		
		convertButton.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent event){
				if (!binaryField.getText().equals("") && decimalField.getText().equals("")){
					decimalField.setText(String.valueOf(math.binaryToDecimal(binaryField.getText())));
				}else if (!decimalField.getText().equals("") && binaryField.getText().equals("")){
					binaryField.setText(math.decimalToBinary(decimalField.getText()));
				}else{
					decimalField.setText(String.valueOf(math.binaryToDecimal(binaryField.getText())));
				}
			}
		});
	}
}