package me.choco.ski.utils;

import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;

import me.choco.ski.SkiGame;

public class ImageUtils {
	
	/**
	 * Attempt to load an image from a relative path
	 * 
	 * @param path - The path to load from
	 * @return the buffered image. Null if none found
	 */
	public static BufferedImage loadImage(String path) {
		try{
			return ImageIO.read(ImageUtils.class.getResource(path));
		}catch(IOException e){
			SkiGame.LOGGER.severe("Could not load resource at path " + path);
			return null;
		}
	}
	
}