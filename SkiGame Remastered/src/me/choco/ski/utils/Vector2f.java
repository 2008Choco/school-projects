package me.choco.ski.utils;

/**
 * A basic mathematical vector
 */
public class Vector2f {
	
	// A zero vector. It's a utility vector, really
	private static final Vector2f ZERO = new Vector2f(0, 0);
	
	// Vector data
	private float x, y;
	
	public Vector2f(float x, float y) {
		this.x = x;
		this.y = y;
	}
	
	/**
	 * Set the x value of the vector
	 * 
	 * @param x - The x value to set
	 * @return itself
	 */
	public Vector2f setX(float x) {
		this.x = x;
		return this;
	}
	
	/**
	 * Get the x value of the vector
	 * 
	 * @return the x value
	 */
	public float getX() {
		return x;
	}
	
	/**
	 * Set the y value of the vector
	 * 
	 * @param y - The y value to set
	 * @return itself
	 */
	public Vector2f setY(float y) {
		this.y = y;
		return this;
	}
	
	/**
	 * Get the y value of the vector
	 * 
	 * @return the y value
	 */
	public float getY() {
		return y;
	}
	
	/**
	 * Get the magnitude of the vector
	 * 
	 * @return the magnitude
	 */
	public float getMagnitude() {
		return (float) StrictMath.sqrt(this.getMagnitudeSquared());
	}
	
	/**
	 * Get the magnitude of the vector squared. This is
	 * a more resource-friendly method to obtain the magnitude, as
	 * {@link Math#sqrt(double)} can be quite resource-heavy
	 * 
	 * @return the magnitude squared
	 */
	public float getMagnitudeSquared() {
		return (x * x) + (y * y);
	}
	
	/**
	 * Add another vector the current vector
	 * 
	 * @param vector - The vector to add
	 * @return the current vector
	 */
	public Vector2f add(Vector2f vector) {
		return this.setX(x + vector.x).setY(y + vector.y);
	}
	
	/**
	 * Subtract another vector the current vector
	 * 
	 * @param vector - The vector to subtract
	 * @return the current vector
	 */
	public Vector2f subtract(Vector2f vector) {
		return this.setX(x - vector.x).setY(y - vector.y);
	}
	
	/**
	 * Multiply the current vector by a scalar value
	 * 
	 * @param scalar - The scalar value to multiply by
	 * @return itself
	 */
	public Vector2f multiply(float scalar) {
		return this.setX(x * scalar).setY(y * scalar);
	}
	
	/**
	 * Divide the current vector by a scalar value
	 * 
	 * @param scalar - The scalar value to divide by
	 * @return itself
	 */
	public Vector2f divide(float scalar) {
		return this.setX(x / scalar).setY(y / scalar);
	}
	
	/**
	 * Normalize the vector
	 * 
	 * @return a unit vector
	 */
	public Vector2f normalize() {
		float magnitude = this.getMagnitude();
		return this.setX(x / magnitude).setY(y / magnitude);
	}
	
	/**
	 * Get a zero vector with an x and y value of 0
	 * 
	 * @return a zero'd vector
	 */
	public static Vector2f zero() {
		return ZERO.clone();
	}
	
	@Override
	public Vector2f clone() {
		return new Vector2f(x, y);
	}
}