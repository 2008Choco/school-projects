package me.choco.ski.utils.timers;

import java.util.Random;

import me.choco.ski.GamePanel;
import me.choco.ski.SkiGame;
import me.choco.ski.entity.Flag;
import me.choco.ski.entity.Tree;
import me.choco.ski.textures.Background;
import me.choco.ski.textures.Texture;
import me.choco.ski.utils.EntityHandler;
import me.choco.ski.utils.Vector2f;

public class TimedEntitySpawner implements Runnable {
	
	private static final Random random = new Random();
	
	private final Background background;
	private final EntityHandler entityHandler;
	public TimedEntitySpawner(GamePanel game) {
		this.background = game.getBackgroundRenderer();
		this.entityHandler = game.getEntityHandler();
	}
	
	@Override
	public void run() {
		float scrollSpeed = this.background.getScrollSpeed();
		
		// Tree spawning (17% chance)
		if (random.nextFloat() * (6 / scrollSpeed) <= 1){
			int width = Texture.SPRITESHEET_TREE.getSpriteWidth();
			Tree tree = new Tree(new Vector2f(random.nextInt(SkiGame.WIDTH - width) + width, SkiGame.HEIGHT));
			tree.setVelocity(0, -scrollSpeed);
			
			this.entityHandler.addEntity(tree);
		}
		
		// Flag spawning (6.25% chance)
		if (random.nextFloat() * (16 / scrollSpeed) <= 1){
			int width = Texture.SPRITESHEET_FLAG.getSpriteWidth();
			Flag flag = new Flag(new Vector2f(random.nextInt(SkiGame.WIDTH - width) + width, SkiGame.HEIGHT));
			flag.setVelocity(0, -scrollSpeed);
			
			this.entityHandler.addEntity(flag);
		}
	}
}