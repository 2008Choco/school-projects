package me.choco.ski.utils;

import java.awt.Graphics2D;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import me.choco.ski.entity.Entity;

public class EntityHandler {
	
	// This holds all entities. Used to easily access them at any time
	private final List<Entity> entities = new ArrayList<>();
	
	/**
	 * Add an entity to the game
	 * 
	 * @param entity - The entity to add
	 */
	public void addEntity(Entity entity) {
		this.entities.add(entity);
	}
	
	/**
	 * Remove an entity from the game
	 * 
	 * @param entity - The entity to remove
	 */
	public void removeEntity(Entity entity) {
		this.entities.remove(entity);
	}
	
	@SuppressWarnings("unchecked")
	/**
	 * Get a list of entities of the specified type, T
	 * 
	 * @param clazz - The type of entity to find
	 * @return all entities of the specified type
	 */
	public <T extends Entity> List<T> getEntities(Class<T> clazz) {
		return (List<T>) this.entities.stream().filter(e -> (clazz.isInstance(e))).collect(Collectors.toList());
	}
	
	@SafeVarargs
	/**
	 * Get a list of entities of the specified types
	 * 
	 * @param clazz - The types of entities to find
	 * @return all entities of the specified types
	 */
	public final List<Entity> getEntities(Class<? extends Entity>... classes) {
		return this.entities.stream().filter(e -> {
			for (Class<? extends Entity> clazz : classes)
				if (clazz.isInstance(e)) return true;
			return false;
		}).collect(Collectors.toList());
	}
	
	/**
	 * Get a list of all entities in the game
	 * 
	 * @return a list of all entities
	 */
	public List<Entity> getEntities() {
		return entities;
	}
	
	/**
	 * Update all entities in the registry. See {@link Entity#tick()}
	 */
	public void tick() {
		this.entities.forEach(e -> e.tick());
	}
	
	/**
	 * Render all entities in the registry. See {@link Entity#render(Graphics2D)}
	 * 
	 * @param g - The graphics context to render to
	 */
	public void render(Graphics2D g) {
		this.entities.forEach(e -> e.render(g));
	}
	
	/**
	 * Clear the registry of all entities
	 */
	public void clearEntities() {
		this.clearEntities();
	}
}