package me.choco.ski;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.util.Random;
import java.util.logging.Logger;

import javax.swing.JFrame;

public class SkiGame extends JFrame {
	
	private static final long serialVersionUID = -1332803247592023042L;
	
	private static SkiGame instance;

	// Constants required for the game
	public static final Random RANDOM = new Random();
	public static final Logger LOGGER = Logger.getLogger("SkiGame");
	private static final JFrame FRAME = new JFrame("SkiGame");
	private static final Dimension SCREEN_SIZE = Toolkit.getDefaultToolkit().getScreenSize();
	
	public static int WIDTH = (int) Math.round(SCREEN_SIZE.getWidth() / 1.6);
	public static int HEIGHT = (int) Math.round((SCREEN_SIZE.getHeight() / 2) + SCREEN_SIZE.getHeight() / 4.5);
	
	public static final int MAX_FPS = 60, MAX_UPS = 20;
	
	// Game logic / information
	private boolean running = true;
	private final GamePanel gamePanel;

	// Initial construction of the game
	private SkiGame() {
		instance = this;
		this.gamePanel = new GamePanel();
		
		FRAME.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		FRAME.setSize(WIDTH, HEIGHT);
		FRAME.setContentPane(gamePanel);
		FRAME.setLocationRelativeTo(null);
		FRAME.setVisible(true);
		
		FRAME.addComponentListener(new ComponentListener() {
			@Override
			public void componentResized(ComponentEvent event) {
				Component component = event.getComponent();
				if (component == null) return;
				
				int newWidth = component.getWidth(), newHeight = component.getHeight();
				
				if (newWidth != WIDTH) WIDTH = component.getWidth();
				if (newHeight != HEIGHT) HEIGHT = component.getHeight();
				
				gamePanel.updateImage();
			}
			
			public void componentHidden(ComponentEvent event) {}
			public void componentMoved(ComponentEvent event) {}
			public void componentShown(ComponentEvent event) {}
		});
		
		gamePanel.run();
	}
	
	/** 
	 * Set whether the game is running or not
	 * 
	 * @param running - the state of the game
	 */
	public void setRunning(boolean running) {
		this.running = running;
	}
	
	/** 
	 * Check whether the game is running or not. If this method
	 * is able to be called, it's likely going to return true
	 * 
	 * @return whether the game is running or not
	 */
	public boolean isRunning() {
		return running;
	}
	
	/**
	 * Get an instance of the SkiGame class through static-means
	 * <br>This is meant to be used for utility purposes, and not static-abuse
	 * 
	 * @return an instance of SkiGame
	 */
	public static SkiGame getSkiGame() {
		return instance;
	}
	
	/** 
	 * Get an instance of the GamePanel class
	 * 
	 * @return the GamePanel instance
	 */
	public GamePanel getGamePanel() {
		return gamePanel;
	}
	
	public static void main(String[] args) { new SkiGame(); }
}