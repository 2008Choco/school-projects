package me.choco.ski.input;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class KeyboardInput implements KeyListener {
	
	// Keep track of what keys are pressed
	private boolean[] keyPressed = new boolean[256];
	
	@Override
	public void keyPressed(KeyEvent e) {
		// When pressed, set it to true
		keyPressed[e.getKeyCode()] = true; 
	}

	@Override
	public void keyReleased(KeyEvent e) {
		// When released, set it to false
		keyPressed[e.getKeyCode()] = false;
	}
	
	/**
	 * Check whether a specific key is pressed. Key integer values
	 * should be specified by the {@link KeyEvent} class
	 * 
	 * @param key - The key to check
	 * @return true if the key is pressed
	 */
	public boolean isKeyPressed(int key) {
		return this.keyPressed[key];
	}
	
	/**
	 * Check whether a specific key is released. Key integer values
	 * should be specified by the {@link KeyEvent} class
	 * 
	 * @param key - The key to check
	 * @return true if the key is released
	 */
	public boolean isKeyReleased(int key) {
		return !this.keyPressed[key];
	}

	
	// This method does not get used by the game. It reacts similarly to #keyPressed() anyways
	@Override
	public void keyTyped(KeyEvent e) {}
}