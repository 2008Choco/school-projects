package me.choco.ski;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import javax.swing.JPanel;

import me.choco.ski.entity.Entity;
import me.choco.ski.entity.Flag;
import me.choco.ski.entity.Player;
import me.choco.ski.entity.Tree;
import me.choco.ski.input.KeyboardInput;
import me.choco.ski.textures.Background;
import me.choco.ski.textures.Texture;
import me.choco.ski.utils.EntityHandler;
import me.choco.ski.utils.Vector2f;
import me.choco.ski.utils.timers.TimedEntitySpawner;

public class GamePanel extends JPanel {
	
	// Graphics based fields
	private Graphics2D graphics;
	private BufferedImage graphicsBuffer;
	
	private Background background;

	// Informational fields
	private static final long serialVersionUID = 2004391125910235354L;
	private int currentFPS = 0, currentUPS = 0;
	
	// Manager & handler fields
	private EntityHandler entityHandler;
	private KeyboardInput keyboardInput;
	
	private ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
	
	/**
	 * Initialize all necessary fields and data required
	 * for the game to start. This MUST be called before any
	 * game logic is actually executed
	 */
	public void init() {
		// GUI-related data initialization
		this.requestFocus();
		this.updateImage();
		
		// Loading & registering various data
		Texture.loadTextures();
		this.entityHandler = new EntityHandler();
		this.background = new Background(this, Texture.TEXTURE_BACKGROUND);
		this.addKeyListener(keyboardInput = new KeyboardInput());
		
		this.entityHandler.addEntity(new Player(new Vector2f(SkiGame.WIDTH / 2, 20)));
		
		// Schedule required tasks
		scheduler.scheduleAtFixedRate(new TimedEntitySpawner(this), 10, 200, TimeUnit.MILLISECONDS);
	}
	
	/**
	 * Run the game, commence initialization, updating and rendering
	 */
	public void run() {
		this.init();
		
		// Game-loop variables
		long startTime = System.currentTimeMillis();
		long lastTimeUPS = System.nanoTime(), lastTimeFPS = System.nanoTime();
		double deltaFPS = 0.0, deltaUPS = 0.0;
		double nsFPS = 1_000_000_000.0 / SkiGame.MAX_FPS, nsUPS = 1_000_000_000.0 / SkiGame.MAX_UPS;
		int fps = 0, ups = 0;
		
		// Main game loop. Will run as long as the game is capable of running
		while (SkiGame.getSkiGame().isRunning()) {
			// Game updates
			long now = System.nanoTime();
			deltaUPS += (now - lastTimeUPS) / nsUPS;
			lastTimeUPS = now;

			if (deltaUPS >= 1.0) {
				this.update();
				ups++;
				deltaUPS--;
			}
			
			// Frame render updates
			now = System.nanoTime();
			deltaFPS += (now - lastTimeFPS) / nsFPS;
			lastTimeFPS = now;
			
			if (deltaFPS >= 1.0) {
				this.render();
				fps++;
				deltaFPS--;
			}

			// FPS and UPS display
			if (System.currentTimeMillis() - startTime > 1000) {
				startTime += 1000;
				System.out.println("SkiGame - (FPS: " + this.currentFPS + " | UPS: " + this.currentUPS + ")");
				this.currentFPS = fps;
				this.currentUPS = ups;
				fps = 0;
				ups = 0;
			}
		}

		this.cleanup();
	}
	
	/**
	 * Update the game and tick entities
	 */
	public void update() {
		Player player = this.entityHandler.getEntities(Player.class).get(0);
		if (player != null && !player.isDead()) {
			this.background.update();
			this.entityHandler.tick();
			
			// Check bounding boxes
			List<Entity> entities = this.entityHandler.getEntities().stream().filter(e -> !(e instanceof Player)).collect(Collectors.toList());
			
			if (entities.size() > 1) {
				entities.forEach(e -> {
					if (player.collides(e)) {
						if (e instanceof Tree) {
							player.kill();
						}
						else if (e instanceof Flag) {
							player.addScore(((Flag) e).getPointValue());
							this.entityHandler.removeEntity(e);
						}
					}
				});
			}
		} else if (player.isDead() && player.shouldDoFinalRender()) {
			player.tick();
			player.finishRendering();
		}
	}
	
	/**
	 * Render the game and all entities
	 */
	public void render() {
		// Clear previous graphics
		graphics.clearRect(0, 0, SkiGame.WIDTH, SkiGame.HEIGHT);
		this.background.render(this.graphics);
		
		// Draw graphics
		this.entityHandler.render(this.graphics);
		
		// Draw FPS, UPS & Score
		graphics.setColor(Color.YELLOW);
		graphics.drawString("FPS: " + this.currentFPS, 3, 15);
		graphics.drawString("UPS: " + this.currentUPS, 3, 30);
		
		graphics.setColor(Color.GREEN);
		String scoreDisplay = "Score: " + this.entityHandler.getEntities(Player.class).get(0).getScore();
		int scoreWidth = graphics.getFontMetrics().stringWidth(scoreDisplay);
		graphics.drawString(scoreDisplay, SkiGame.WIDTH - scoreWidth - 25, 15);
		
		// Draw new graphics to screen
		Graphics panelGraphics = this.getGraphics();
		panelGraphics.drawImage(graphicsBuffer, 0, 0, SkiGame.WIDTH, SkiGame.HEIGHT, null);
		panelGraphics.dispose();
	}
	
	/**
	 * Cleanup any resources that require to be closed when 
	 * the game shuts down
	 */
	public void cleanup() {
		this.graphics.dispose();
		this.entityHandler.clearEntities();
		this.scheduler.shutdown();
	}
	
	/**
	 * Update the graphics buffer and prepare for a new render ratio
	 */
	public void updateImage() {
		if (graphics != null) this.graphics.dispose();
		
		this.graphicsBuffer = new BufferedImage(SkiGame.WIDTH, SkiGame.HEIGHT, BufferedImage.TYPE_INT_RGB);
		this.graphics = (Graphics2D) graphicsBuffer.getGraphics();
		graphics.setFont(new Font("Arial", Font.BOLD, 17));
	}
	
	/**
	 * Get an instance of the entity handler
	 * 
	 * @return the entity handler
	 */
	public EntityHandler getEntityHandler() {
		return entityHandler;
	}
	
	/**
	 * Get an instance of the keyboard input handler
	 * 
	 * @return the keyboard handler
	 */
	public KeyboardInput getKeyboardInput() {
		return keyboardInput;
	}
	
	/**
	 * Get an instance of the background manager
	 * 
	 * @return the background manager
	 */
	public Background getBackgroundRenderer() {
		return background;
	}
}