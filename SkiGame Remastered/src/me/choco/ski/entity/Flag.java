package me.choco.ski.entity;

import me.choco.ski.SkiGame;
import me.choco.ski.textures.Texture;
import me.choco.ski.utils.Vector2f;

public class Flag extends Entity {
	
	// Flag data
	private final int pointValue;
	
	// A flag with a point value
	public Flag(Vector2f position, int pointValue) {
		super(position, Texture.SPRITESHEET_FLAG, SkiGame.RANDOM.nextInt(Texture.SPRITESHEET_FLAG.getSpriteCount()));
		this.pointValue = pointValue;
	}
	
	// A flag with a random point value (between 1 and 3)
	public Flag(Vector2f position) {
		this(position, SkiGame.RANDOM.nextInt(3) + 1);
	}
	
	/**
	 * Get the point value that this flag will yield
	 * 
	 * @return the point value of the flag
	 */
	public int getPointValue() {
		return pointValue;
	}
}