package me.choco.ski.entity;

import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;

import me.choco.ski.textures.SpriteSheet;
import me.choco.ski.utils.Vector2f;

/**
 * The base of all entity objects. Tickable and renderable objects or
 * controllable entities are all considered to be an "Entity"
 */
public class Entity {
	
	// Currently active sprite
	protected int activeSprite = 0;
	
	// Rendering & movement information
	protected Vector2f position, velocity, acceleration;
	protected int width, height;
	protected final BufferedImage[] sprites;
	
	// An entity with a single texture, and no sprite sheet
	public Entity(Vector2f position, BufferedImage sprite) {
		this.position = position;
		this.velocity = new Vector2f(0, 0);
		this.acceleration = new Vector2f(0, 0);
		
		this.sprites = new BufferedImage[]{ sprite };
		
		this.width = sprite.getWidth();
		this.height = sprite.getHeight();
		
	}
	
	// An entity with multiple textures (i.e. a spritesheet) with an initial texture and width / height
	public Entity(Vector2f position, SpriteSheet spritesheet, int activeSprite, int width, int height) {
		this.position = position;
		this.velocity = new Vector2f(0, 0);
		this.acceleration = new Vector2f(0, 0);
		
		this.sprites = spritesheet.getSprites();
		this.activeSprite = activeSprite;

		this.width = width;
		this.height = height;
	}
	
	// An entity with multiple textures (i.e. a spritesheet) with an initial texture
	public Entity(Vector2f position, SpriteSheet spritesheet, int activeSprite) {
		this.position = position;
		this.velocity = new Vector2f(0, 0);
		this.acceleration = new Vector2f(0, 0);
		
		this.sprites = spritesheet.getSprites();
		this.activeSprite = activeSprite;
		
		BufferedImage sprite = this.sprites[activeSprite];
		this.width = sprite.getWidth();
		this.height = sprite.getHeight();
	}
	
	// An entity with multiple textures (i.e. a spritesheet)
	public Entity(Vector2f position, SpriteSheet spritesheet) {
		this(position, spritesheet, 0);
	}
	
	/**
	 * Set the position of the entity
	 * 
	 * @param x - The x coordinate to set
	 * @param y - The y coordinate to set
	 */
	public void setPosition(float x, float y) {
		this.position.setX(x);
		this.position.setY(y);
	}
	
	/**
	 * Set the position of the entity
	 * 
	 * @param position - The position to set
	 */
	public void setPosition(Vector2f position) {
		this.position = position;
	}
	
	/**
	 * Get the current position of the entity
	 * 
	 * @return the entity's position
	 */
	public Vector2f getPosition() {
		return position;
	}
	
	/**
	 * Set the currently active sprite to be rendered for
	 * this entity
	 * 
	 * @param activeSprite - The sprite index to set
	 */
	public void setActiveSprite(int activeSprite) {
		if (activeSprite < 0) activeSprite = 0;
		if (activeSprite >= this.sprites.length) activeSprite = (this.sprites.length - 1);
		
		this.activeSprite = activeSprite;
	}
	
	/**
	 * Get the active sprite index for this entity
	 * 
	 * @returnt the active sprite
	 */
	public int getActiveSprite() {
		return activeSprite;
	}
	
	/**
	 * Get the current sprite used by this entity when rendererd
	 * 
	 * @return the current sprite
	 */
	public BufferedImage getSprite() {
		return sprites[activeSprite];
	}
	
	/**
	 * Get an array of all sprites usable by this entity
	 * 
	 * @return all entity sprites
	 */
	public BufferedImage[] getSprites() {
		return sprites;
	}
	
	/**
	 * Set the velocity of the entity. The velocity will be
	 * added to the entity's location every tick
	 * 
	 * @param x - The x value of the velocity
	 * @param y - The y value of the velocity
	 */
	public void setVelocity(float x, float y) {
		this.velocity.setX(x);
		this.velocity.setY(y);
	}
	
	/**
	 * Set the velocity of the entity. The velocity will be
	 * added to the entity's location every tick
	 * 
	 * @param velocity - The velocity to set
	 */
	public void setVelocity(Vector2f velocity) {
		this.velocity = velocity;
	}
	
	/**
	 * Get the current velocity for this entity
	 * 
	 * @return the entity's velocity
	 */
	public Vector2f getVelocity() {
		return velocity;
	}
	
	/**
	 * Set the acceleration of the entity. The acceleration will 
	 * be added to the entity's velocity every tick
	 * 
	 * @param x - The x value of the acceleration
	 * @param y - The y value of the acceleration
	 */
	public void setAcceleration(float x, float y) {
		this.velocity.setX(x);
		this.velocity.setY(y);
	}
	
	/**
	 * Set the acceleration of the entity. The acceleration will 
	 * be added to the entity's velocity every tick
	 * 
	 * @param acceleration - The acceleration to set
	 */
	public void setAcceleration(Vector2f acceleration) {
		this.acceleration = acceleration;
	}
	
	/**
	 * Get the current acceleration for this entity
	 * 
	 * @return the entity's acceleration
	 */
	public Vector2f getAcceleration() {
		return acceleration;
	}
	
	/**
	 * Set the width of the entity's bounding box
	 * <br><b>NOTE: </b>This width will not affect the width
	 * in which the entity will get rendered
	 * 
	 * @param width - The width to set
	 */
	public void setWidth(int width) {
		this.width = width;
	}
	
	/**
	 * Get the current width of the entity's bounding box
	 * 
	 * @return the width of the bounding box
	 */
	public int getWidth() {
		return width;
	}

	/**
	 * Set the height of the entity's bounding box
	 * <br><b>NOTE: </b>This width will not affect the height
	 * in which the entity will get rendered
	 * 
	 * @param height - The height to set
	 */
	public void setHeight(int height) {
		this.height = height;
	}
	
	/**
	 * Get the current height of the entity's bounding box
	 * 
	 * @return the height of the bounding box
	 */
	public int getHeight() {
		return height;
	}
	
	/**
	 * Get the entity's bounding box
	 * 
	 * @return the bounding box
	 */
	public Rectangle getBoundingBox() {
		int spriteDiffX = (this.getSprite().getWidth() - width) / 2;
		int spriteDiffY = (this.getSprite().getHeight() - height) / 2;
		return new Rectangle((int) position.getX() + spriteDiffX, (int) position.getY() + spriteDiffY, width, height);
	}
	
	/**
	 * Check whether this and another entity collide
	 * 
	 * @param entity - The entity to check
	 * @return true if the entities collide
	 */
	public boolean collides(Entity entity) {
		return this.getBoundingBox().intersects(entity.getBoundingBox());
	}
	
	/**
	 * Update the entity. This will be called for every update the
	 * game needs to make every second. (See GamePanel#MAX_UPS)
	 */
	public void tick() {
		this.position.add(velocity);
		this.velocity.add(acceleration);
	}
	
	/**
	 * Render the entity to the screen. This will be called for every
	 * rendering call the game needs to make every second. 
	 * <br>
	 * <br>No updating logic should be called in this method due to the 
	 * fact that this can fluctuate depending on the speed of the 
	 * computer running the game. (See GamePanel#MAX_FPS)
	 * 
	 * @param g - The graphics context to render to
	 */
	public void render(Graphics2D g) {
		g.drawImage(this.getSprite(), (int) position.getX(), (int) position.getY(), null);
	}
	
}