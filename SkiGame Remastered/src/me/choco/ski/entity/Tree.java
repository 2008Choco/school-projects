package me.choco.ski.entity;

import me.choco.ski.SkiGame;
import me.choco.ski.textures.Texture;
import me.choco.ski.utils.Vector2f;

public class Tree extends Entity {
	
	public Tree(Vector2f position) {
		super(position, Texture.SPRITESHEET_TREE, SkiGame.RANDOM.nextInt(Texture.SPRITESHEET_TREE.getSpriteCount()));
	}
	
}