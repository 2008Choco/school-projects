package me.choco.ski.entity;

import com.sun.glass.events.KeyEvent;

import me.choco.ski.SkiGame;
import me.choco.ski.input.KeyboardInput;
import me.choco.ski.textures.Background;
import me.choco.ski.textures.Texture;
import me.choco.ski.utils.Vector2f;

public class Player extends Entity {
	
	// Sprite index positions
	public static final int SPRITE_RIGHT_RIGHT_TURN = 0;
	public static final int SPRITE_RIGHT_TURN = 1;
	public static final int SPRITE_FORWARD = 2;
	public static final int SPRITE_LEFT_TURN = 3;
	public static final int SPRITE_LEFT_LEFT_TURN = 4;
	public static final int SPRITE_FALLEN = 5;

	// Required static fields
	private static final KeyboardInput keyInput = SkiGame.getSkiGame().getGamePanel().getKeyboardInput();
	private static final Background background = SkiGame.getSkiGame().getGamePanel().getBackgroundRenderer();
	private static final float MAX_SPEED = 3.0f;
	
	// Player data
	private boolean dead = false;
	private boolean finalRender = true;
	private int score = 0;
	
	public Player(Vector2f position) {
		super(position, Texture.SPRITESHEET_PLAYER, SPRITE_FORWARD);
	}
	
	@Override
	public void tick() {
		super.tick();
		
		if (!dead) {
			// Keyboard checks
			if (keyInput.isKeyPressed(KeyEvent.VK_W)) background.setScrollAcceleration(-0.1f);
			else if (keyInput.isKeyPressed(KeyEvent.VK_S)) background.setScrollAcceleration(0.1f);
			else background.setScrollAcceleration(0);
			
			if (keyInput.isKeyPressed(KeyEvent.VK_A)) this.acceleration.setX(-0.1f);
			else if (keyInput.isKeyPressed(KeyEvent.VK_D)) this.acceleration.setX(0.1f);
			else this.acceleration.setX(0);
			
			// Velocity movement
			float x = this.velocity.getX();
			if (x > MAX_SPEED) this.velocity.setX(MAX_SPEED);
			if (x < -MAX_SPEED) this.velocity.setX(-MAX_SPEED);
			
			if (x > -0.75 && x < 0.75) {
				this.activeSprite = SPRITE_FORWARD;
				this.width = 30;
				this.height = 57;
			}
			else if (x >= 0.75 && x < 2.25) {
				this.activeSprite = SPRITE_RIGHT_TURN;
				this.width = 32;
				this.height = 60;
			}
			else if (x >= 2.25) {
				this.activeSprite = SPRITE_RIGHT_RIGHT_TURN;
				this.width = 43;
				this.height = 56;
			}
			else if (x <= -0.75 && x > -2.25) {
				this.activeSprite = SPRITE_LEFT_TURN;
				this.width = 32;
				this.height = 60;
			}
			else if (x <= -2.25) {
				this.activeSprite = SPRITE_LEFT_LEFT_TURN;
				this.width = 43;
				this.height = 60;
			}
		}
		else {
			this.activeSprite = SPRITE_FALLEN;
		}
	}
	
	/**
	 * Kill the player. This should theoretically end the game all-together
	 */
	public void kill() {
		// TODO: Stop the background from scrolling
		this.dead = true;
	}
	
	/**
	 * Check whether the player is dead or not
	 * 
	 * @return true if the player is dead
	 */
	public boolean isDead() {
		return dead;
	}
	
	/**
	 * Whether a final render call should be made on the player or not
	 * 
	 * @return true if final render to be made
	 */
	public boolean shouldDoFinalRender() {
		return this.finalRender;
	}
	
	/**
	 * Complete the final rendering stage
	 */
	public void finishRendering() {
		this.finalRender = false;
	}
	
	/**
	 * Add a positive score value to the player
	 * 
	 * @param score - The amount to add
	 */
	public void addScore(int score) {
		if (score <= 0) return;
		this.score += score;
	}
	
	/**
	 * Remove a positive score value from the player
	 * 
	 * @param score - The amount to remove
	 */
	public void removeScore(int score) {
		if (score <= 0) return;
		this.score -= score;
	}
	
	/**
	 * Get the player's current score
	 * 
	 * @return the score
	 */
	public int getScore() {
		return score;
	}
	
}