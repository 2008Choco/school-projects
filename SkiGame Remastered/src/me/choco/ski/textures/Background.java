package me.choco.ski.textures;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;

import me.choco.ski.GamePanel;
import me.choco.ski.SkiGame;
import me.choco.ski.entity.Flag;
import me.choco.ski.entity.Tree;
import me.choco.ski.utils.EntityHandler;

public class Background {
	
	// Required static fields (General information for the update method)
	private static final int BACKGROUND_BUFFER = 2, MAX_BACKGROUND_ACCELERATION = 4;
	
	// Background data & acceleration information
	private float scrollOffset = 0, scrollSpeed = 1, scrollAcceleration = 0;
	private final BufferedImage background;
	
	private final EntityHandler entityHandler;
	
	public Background(GamePanel game, BufferedImage background) {
		this.entityHandler = game.getEntityHandler();
		this.background = background;
	}
	
	/**
	 * Set the value at which the background will continually scroll at
	 * 
	 * @param scrollAcceleration - The acceleration of the background scroll
	 */
	public void setScrollAcceleration(float scrollAcceleration) {
		this.scrollAcceleration = scrollAcceleration;
	}
	
	/**
	 * Get the acceleration of the background scroll
	 * 
	 * @return the background acceleration
	 */
	public float getScrollAcceleration() {
		return scrollAcceleration;
	}
	
	/**
	 * Set the speed at which the background is scrolling
	 * 
	 * @param scrollSpeed - The speed to set. Default is 1
	 */
	public void setScrollSpeed(float scrollSpeed) {
		this.scrollSpeed = scrollSpeed;
	}
	
	/**
	 * Get the current speed at which the background is scrolling
	 * 
	 * @return the scroll speed
	 */
	public float getScrollSpeed() {
		return scrollSpeed;
	}
	
	/**
	 * Update the entity. This will be called for every update the
	 * game needs to make every second. (See GamePanel#MAX_UPS). Updates
	 * for the background are limited to increasing the scroll offset
	 */
	public void update() {
		float previousScrollSpeed = this.scrollSpeed;
		
		// Acceleration & velocity updates
		this.scrollSpeed += this.scrollAcceleration;
		if (this.scrollSpeed < 1) this.scrollSpeed = 1;
		else if (this.scrollSpeed > MAX_BACKGROUND_ACCELERATION) this.scrollSpeed = MAX_BACKGROUND_ACCELERATION;
		
		this.scrollOffset += this.scrollSpeed;
		if (this.scrollSpeed != previousScrollSpeed) 
			entityHandler.getEntities(Tree.class, Flag.class).forEach(e -> e.setVelocity(e.getVelocity().getX(), -this.scrollSpeed));
		
		// Reset rendering offset after an entire image has passed by the screen
		if (scrollOffset >= SkiGame.HEIGHT) scrollOffset = 0;
	}
	
	/**
	 * Render the background to the screen. This will be called for every
	 * rendering call the game needs to make every second. 
	 * <br>
	 * <br>No updating logic should be called in this method due to the 
	 * fact that this can fluctuate depending on the speed of the 
	 * computer running the game. (See GamePanel#MAX_FPS)
	 * 
	 * @param g - The graphics context to render to
	 */
	public void render(Graphics2D g) {
		for (int i = 0; i < BACKGROUND_BUFFER; i++) 
			g.drawImage(background, 0, (i * SkiGame.HEIGHT) - (int) scrollOffset, SkiGame.WIDTH, SkiGame.HEIGHT, null);
	}
}