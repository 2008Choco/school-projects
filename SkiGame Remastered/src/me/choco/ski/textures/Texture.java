package me.choco.ski.textures;

import java.awt.image.BufferedImage;

import me.choco.ski.utils.ImageUtils;

/**
 * Assist in the management of textures and sprite sheets. 
 * This class assures that only a single instance of textures
 * are loaded to memory
 */
public class Texture {
	
	// Single textures
	public static BufferedImage TEXTURE_BACKGROUND;
	
	// Sprite sheets
	public static SpriteSheet SPRITESHEET_TREE;
	public static SpriteSheet SPRITESHEET_PLAYER;
	public static SpriteSheet SPRITESHEET_FLAG;
	
	/**
	 * Load all textures from file to memory. This method must be
	 * called before accessing any fields; otherwise they will be null
	 */
	public static void loadTextures() {
		TEXTURE_BACKGROUND = ImageUtils.loadImage("/background.png");
		
		SPRITESHEET_TREE = new SpriteSheet(ImageUtils.loadImage("/skier_tree.png"), 42, 48);
		SPRITESHEET_PLAYER = new SpriteSheet(ImageUtils.loadImage("/skier_player.png"), 64, 64);
		SPRITESHEET_FLAG = new SpriteSheet(ImageUtils.loadImage("/skier_flag.png"), 12, 24);
	}
	
}