package me.choco.ski.textures;

import java.awt.image.BufferedImage;
import java.awt.image.RasterFormatException;

import me.choco.ski.SkiGame;

/**
 * Represents a collection of sprites in a single image. Sprite sheets
 * are parsable and contain various sprites to aleviate the overhead of 
 * loading multiple textures. 
 * 
 * Rather than loading x amount of sprites, it's more efficient to load 
 * a single texture and parse x amount of sprites out of that image.
 */
public class SpriteSheet {
	
	// Sprite data
	private final BufferedImage sheet;
	private final int spriteWidth, spriteHeight;
	
	private final BufferedImage[] sprites;
	
	public SpriteSheet(BufferedImage sheet, int spriteWidth, int spriteHeight) {
		this.sheet = sheet;
		this.spriteWidth = spriteWidth;
		this.spriteHeight = spriteHeight;
		
		// Sprite information. Used for sprite parsing
		int sheetWidth = sheet.getWidth(), sheetHeight = sheet.getHeight();
		int xSpriteCount = sheetWidth / spriteWidth, ySpriteCount = sheetHeight / spriteHeight;
		
		this.sprites = new BufferedImage[xSpriteCount * ySpriteCount];
		
		// For all possible sprites on the x and y axis
		for (int x = 0; x < xSpriteCount; x++) {
			for (int y = 0; y < ySpriteCount; y++) {
				try {
					// Attempt to set a subimage in the relative index of the sprites array
					BufferedImage sprite = sheet.getSubimage(x * spriteWidth, y * spriteHeight, spriteWidth, spriteHeight);
					this.sprites[(xSpriteCount * y) + x] = sprite;
				}catch(RasterFormatException e) {
					// Otherwise, there is an issue with the scale of the spritesheet
					SkiGame.LOGGER.warning("Could not load spritesheet; " + x + ">" + sheetWidth + " | " + y + ">" + sheetHeight);
					e.printStackTrace();
				}
			}
		}
	}
	
	/**
	 * Get the full sprite sheet image
	 * 
	 * @return the whole sprite sheet
	 */
	public BufferedImage getSheet() {
		return sheet;
	}
	
	/**
	 * Get the width of all sprites on the sprite sheet
	 * 
	 * @return the width of the sprites
	 */
	public int getSpriteWidth() {
		return spriteWidth;
	}
	
	/**
	 * Get the height of all sprites on the sprite sheet
	 * 
	 * @return the height of the sprites
	 */
	public int getSpriteHeight() {
		return spriteHeight;
	}
	
	/**
	 * Get the amount of sprites that were parsed on this sprite sheet
	 * 
	 * @return the amount of sprites
	 */
	public int getSpriteCount() {
		return this.sprites.length;
	}
	
	/**
	 * Get the sprite at the specific index
	 * 
	 * @param index - The index of the sprite to get
	 * @return the sprite at the specified index. Null if out of bounds
	 */
	public BufferedImage getSprite(int index) {
		return index >= 0 && index < this.sprites.length ? this.sprites[index] : null;
	}
	
	/**
	 * Get all sprites that were parsed on the sprite sheet
	 * 
	 * @return all parsed sprites
	 */
	public BufferedImage[] getSprites() {
		return sprites;
	}
}