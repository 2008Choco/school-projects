package me.choco.vector.utils;

/**
 * Represents a Vector in 2D space
 * 
 * @author Parker Hawke - 2008Choco
 */
public class Vector {
	
	private float x, y;
	
	/**
	 * Construct a new vector with given x and y values
	 * 
	 * @param x - The x value of the vector
	 * @param y - The y value of the vector
	 */
	public Vector(float x, float y) {
		this.x = x;
		this.y = y;
	}
	
	/**
	 * Construct a new vector with given x and y values
	 * 
	 * @param x - The x value of the vector
	 * @param y - The y value of the vector
	 */
	public Vector(double x, double y) {
		this.x = (float) x;
		this.y = (float) y;
	}
	
	/**
	 * Construct a new vector with zeroed values (0,0)
	 */
	public Vector() {
		this(0, 0);
	}
	
	/**
	 * Set the x component 
	 * 
	 * @param x - The new X component
	 * @return this vector
	 */
	public Vector setX(float x) {
		this.x = x;
		return this;
	}
	
	/**
	 * Set the x component 
	 * 
	 * @param x - The new X component
	 * @return this vector
	 */
	public Vector setX(double x) {
		this.x = (float) x;
		return this;
	}
	
	/**
	 * Set the x component 
	 * 
	 * @param x - The new X component
	 * @return this vector
	 */
	public Vector setX(int x) {
		this.x = x;
		return this;
	}

	/**
	 * Get the x component
	 * 
	 * @return the X component
	 */
	public float getX() {
		return x;
	}
	
	/**
	 * Set the y component 
	 * 
	 * @param y - The new Y component
	 * @return this vector
	 */
	public Vector setY(float y) {
		this.y = y;
		return this;
	}
	
	/**
	 * Set the y component 
	 * 
	 * @param y - The new Y component
	 * @return this vector
	 */
	public Vector setY(double y) {
		this.y = (float) y;
		return this;
	}
	
	/**
	 * Set the y component 
	 * 
	 * @param y - The new Y component
	 * @return this vector
	 */
	public Vector setY(int y) {
		this.y = y;
		return this;
	}

	/**
	 * Get the Y component
	 * 
	 * @return the X component
	 */
	public float getY() {
		return y;
	}
	
	/**
	 * Adds another vector to this instance
	 * 
	 * @param vector - The vector to add
	 * @return the resultant vector
	 */
	public Vector add(Vector vector) {
		this.x += vector.x;
		this.y += vector.y;
		return this;
	}

	/**
	 * Subtract another vector from this instance
	 * 
	 * @param vector - The vector to subtract
	 * @return the resultant vector
	 */
    public Vector subtract(Vector vector) {
    	this.x -= vector.x;
    	this.y -= vector.y;
        return this;
    }

	/**
	 * Multiply this instance with another vector
	 * 
	 * @param vector - The vector to multiply by
	 * @return the resultant vector
	 */
    public Vector multiply(Vector vector) {
    	this.x *= vector.x;
    	this.y *= vector.y;
        return this;
    }
    
	/**
	 * Multiply this vector with a scalar value
	 * 
	 * @param vector - The scalar to multiply by
	 * @return the resultant vector
	 */
    public Vector multiply(int scalar) {
    	this.x *= scalar;
    	this.y *= scalar;
    	return this;
    }
    
	/**
	 * Multiply this vector with a scalar value
	 * 
	 * @param vector - The scalar to multiply by
	 * @return the resultant vector
	 */
    public Vector multiply(double scalar) {
    	this.x *= scalar;
    	this.y *= scalar;
    	return this;
    }
    
	/**
	 * Multiply this vector with a scalar value
	 * 
	 * @param vector - The scalar to multiply by
	 * @return the resultant vector
	 */
    public Vector multiply(float scalar) {
    	this.x *= scalar;
    	this.y *= scalar;
    	return this;
    }
    
	/**
	 * Divide this instance by another vector
	 * 
	 * @param vector - The vector to divide by
	 * @return the resultant vector
	 */
    public Vector divide(Vector vector) {
    	this.x /= vector.x;
    	this.y /= vector.y;
        return this;
    }
    
	/**
	 * Divide this vector by a scalar value
	 * 
	 * @param vector - The scalar to divide by
	 * @return the resultant vector
	 */
    public Vector divide(int scalar) {
    	this.x /= scalar;
    	this.y /= scalar;
    	return this;
    }
    
	/**
	 * Divide this vector by a scalar value
	 * 
	 * @param vector - The scalar to divide by
	 * @return the resultant vector
	 */
    public Vector divide(double scalar) {
    	this.x /= scalar;
    	this.y /= scalar;
    	return this;
    }
    
	/**
	 * Divide this vector by a scalar value
	 * 
	 * @param vector - The scalar to divide by
	 * @return the resultant vector
	 */
    public Vector divide(float scalar) {
    	this.x /= scalar;
    	this.y /= scalar;
    	return this;
    }
    
    /**
     * Calculate the magnitude of the vector
     * 
     * @return the magnitude
     */
    public double magnitude() {
    	return Math.sqrt(StrictMath.pow(x, 2) + StrictMath.pow(y, 2));
    }
    
    /**
     * Calculate the magnitude of the vector without squaring the value
     * (saves on the overhead of {@link Math#sqrt(double)}
     * 
     * @return the magnitude squared
     */
    public double magnitudeSquared() {
    	return StrictMath.pow(x, 2) + StrictMath.pow(y, 2);
    }
    
    /**
     * Set this vector's components to 0
     * 
     * @return this vector, zeroed
     */
    public Vector zero() {
    	this.x = 0;
    	this.y = 0;
    	return this;
    }
    
    @Override
    public Vector clone() {
    	return new Vector(x, y);
    }
    
}