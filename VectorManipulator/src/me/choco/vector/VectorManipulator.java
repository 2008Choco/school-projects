package me.choco.vector;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;

import javax.swing.JFrame;

public class VectorManipulator {
	
	private static VectorManipulator instance;
	
	public static final int MAX_FPS = 60, MAX_UPS = 20; 
	private static final JFrame FRAME = new JFrame("Vector Manipulator");
	private static final Dimension SCREEN_SIZE = Toolkit.getDefaultToolkit().getScreenSize();
	
	public static int WIDTH = (int) Math.round(SCREEN_SIZE.getWidth() / 1.6);
	public static int HEIGHT = (int) Math.round((SCREEN_SIZE.getHeight() / 2) + SCREEN_SIZE.getHeight() / 4.5);
	
	private boolean running = true;
	private final VectorPanel vectorPanel;
	
	private VectorManipulator() {
		instance = this;
		this.vectorPanel = new VectorPanel();
		
		FRAME.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		FRAME.setSize(WIDTH, HEIGHT);
		FRAME.setContentPane(vectorPanel);
		FRAME.setLocationRelativeTo(null);
		FRAME.setVisible(true);
		
		FRAME.addComponentListener(new ComponentListener() {
			@Override
			public void componentResized(ComponentEvent event) {
				Component component = event.getComponent();
				if (component == null) return;
				
				int newWidth = component.getWidth(), newHeight = component.getHeight();
				
				if (newWidth != WIDTH) WIDTH = component.getWidth();
				if (newHeight != HEIGHT) HEIGHT = component.getHeight();
				
				vectorPanel.updateImage();
			}
			
			public void componentHidden(ComponentEvent event) {}
			public void componentMoved(ComponentEvent event) {}
			public void componentShown(ComponentEvent event) {}
		});
		
		vectorPanel.run();
	}
	
	public static VectorManipulator getInstance() {
		return instance;
	}
	
	public void setRunning(boolean running) {
		this.running = running;
	}
	
	public boolean isRunning() {
		return running;
	}
	
	public static void main(String[] args) { new VectorManipulator(); }
	
}