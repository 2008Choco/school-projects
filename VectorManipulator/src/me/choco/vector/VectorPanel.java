package me.choco.vector;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import java.awt.image.BufferedImage;

import javax.swing.JPanel;

import me.choco.vector.utils.Vector;

public class VectorPanel extends JPanel {

	private static final long serialVersionUID = -3593101517229069234L;
	
	// Graphics based fields
	private Graphics2D graphics;
	private BufferedImage graphicsBuffer;

	private int currentFPS = 0, currentUPS = 0;
	
	private Vector center = new Vector(VectorManipulator.WIDTH / 2, VectorManipulator.HEIGHT / 2);
	private Vector currentPos = new Vector();
	private Vector resultantVector = new Vector();
	
	public void init() {
		this.requestFocus();
		this.updateImage();
		
		this.addMouseMotionListener(new MouseMotionListener() {
			@Override
			public void mouseMoved(MouseEvent event) {
				currentPos.setX(event.getX());
				currentPos.setY(event.getY());
			}
			
			public void mouseDragged(MouseEvent event) {}
		});
	}
	
	public void run() {
		this.init();
		
		// Game-loop variables
		long startTime = System.currentTimeMillis();
		long lastTimeUPS = System.nanoTime(), lastTimeFPS = System.nanoTime();
		double deltaFPS = 0.0, deltaUPS = 0.0;
		double nsFPS = 1_000_000_000.0 / VectorManipulator.MAX_FPS, nsUPS = 1_000_000_000.0 / VectorManipulator.MAX_UPS;
		int fps = 0, ups = 0;
		
		// Main game loop. Will run as long as the game is capable of running
		while (VectorManipulator.getInstance().isRunning()) {
			// Game updates
			long now = System.nanoTime();
			deltaUPS += (now - lastTimeUPS) / nsUPS;
			lastTimeUPS = now;

			if (deltaUPS >= 1.0) {
				this.update();
				ups++;
				deltaUPS--;
			}
			
			// Frame render updates
			now = System.nanoTime();
			deltaFPS += (now - lastTimeFPS) / nsFPS;
			lastTimeFPS = now;
			
			if (deltaFPS >= 1.0) {
				this.render();
				fps++;
				deltaFPS--;
			}

			// FPS and UPS display
			if (System.currentTimeMillis() - startTime > 1000) {
				startTime += 1000;
				System.out.println("Vector Manipulator - (FPS: " + this.currentFPS + " | UPS: " + this.currentUPS + ")");
				this.currentFPS = fps;
				this.currentUPS = ups;
				fps = 0;
				ups = 0;
			}
		}

		this.cleanup();
	}
	
	public void render() {
		this.graphics.clearRect(0, 0, VectorManipulator.WIDTH, VectorManipulator.HEIGHT);
		
		// Draw FPS, UPS & Score
		graphics.setColor(Color.YELLOW);
		graphics.drawString("FPS: " + this.currentFPS, 3, 15);
		graphics.drawString("UPS: " + this.currentUPS, 3, 30);
		
		graphics.setColor(Color.GREEN);
		graphics.drawLine(VectorManipulator.WIDTH / 2, VectorManipulator.HEIGHT / 2,
							(int) ((VectorManipulator.WIDTH / 2) + this.resultantVector.getX()), 
							(int) ((VectorManipulator.HEIGHT / 2) + this.resultantVector.getY()));
		
		// Draw new graphics to screen
		Graphics panelGraphics = this.getGraphics();
		panelGraphics.drawImage(graphicsBuffer, 0, 0, VectorManipulator.WIDTH, VectorManipulator.HEIGHT, null);
		panelGraphics.dispose();
	}
	
	public void update() {
		this.resultantVector = this.currentPos.clone().subtract(center);
	}
	
	public void cleanup() {
		this.graphics.dispose();
	}
	
	/**
	 * Update the graphics buffer and prepare for a new render ratio
	 */
	public void updateImage() {
		if (graphics != null) this.graphics.dispose();
		
		this.graphicsBuffer = new BufferedImage(VectorManipulator.WIDTH, VectorManipulator.HEIGHT, BufferedImage.TYPE_INT_RGB);
		this.graphics = (Graphics2D) graphicsBuffer.getGraphics();
		graphics.setFont(new Font("Arial", Font.BOLD, 17));
	}
	
}