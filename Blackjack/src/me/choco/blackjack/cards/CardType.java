package me.choco.blackjack.cards;

import java.util.Set;

public enum CardType {
	
	ACE("Ace", 1){
		@Override
		public int getValue(Set<Card> hand) {
			int totalValue = -11;
			boolean oneAce = false;
			for (Card card : hand) {
				if (card.getType() == ACE){
					if (!oneAce){
						totalValue += 11;
						oneAce = true;
					}
					else{
						totalValue += 1;
					}
				}
				else{
					totalValue += card.getType().value;
				}
			}
			
			return 21 - totalValue < 11 ? 1 : getValue();
		}
	},
	
	TWO("Two", 2),
	
	THREE("Three", 3),
	
	FOUR("Four", 4),
	
	FIVE("Five", 5),
	
	SIX("Six", 6),
	
	SEVEN("Seven", 7),
	
	EIGHT("Eight", 8),
	
	NINE("Nine", 9),
	
	TEN("Ten", 10),
	
	JACK("Jack", 10),
	
	QUEEN("Queen", 10),
	
	KING("King", 10);
	
	private final int value;
	private final String friendlyName;
	
	private CardType(String friendlyName, int value) {
		this.value = value;
		this.friendlyName = friendlyName;
	}
	
	public String getFriendlyName() {
		return friendlyName;
	}
	
	public int getValue(Set<Card> hand) {
		return this.getValue();
	}
	
	public int getValue() {
		return value;
	}
}