package me.choco.blackjack.cards;

public enum CardSuit {
	
	HEARTS,
	
	SPADES,
	
	CLUBS,
	
	DIAMONDS;
	
}