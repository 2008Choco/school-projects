package me.choco.blackjack.game.utils;

import java.util.ArrayList;
import java.util.List;

import me.choco.blackjack.game.Player;

public class PlayerHandler {
	
	private int currentTurn = 0;

	private final List<Player> players = new ArrayList<>();
	
	public void addPlayer(Player player) {
		this.players.add(player);
	}
	
	public void removePlayer(Player player) {
		this.players.remove(player);
	}
	
	public Player getPlayer(String name) {
		for (Player player : players)
			if (player.getName().equals(name)) return player;
		return null;
	}
	
	public Player getNextTurnPlayer() {
		if (currentTurn >= players.size()) this.currentTurn = 0;
		
		return this.players.get(currentTurn++);
	}
	
	public boolean hasDealerBeenSet() {
		for (Player player : players) 
			if (player.isDealer()) return true;
		return false;
	}

	public List<Player> getPlayers() {
		return players;
	}
	
	public void clearPlayers() {
		this.players.clear();
	}
}