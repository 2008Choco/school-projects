package me.choco.blackjack.game;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import me.choco.blackjack.cards.Card;

public class Player {
	
	private Set<Card> hand = new HashSet<>();
	
	private int wins, loses;
	private boolean busted = false;
	
	private final String name;
	private final boolean npc, dealer;
	
	public Player(String name, boolean npc, boolean dealer) {
		this.name = name;
		this.npc = npc;
		this.dealer = dealer;
	}
	
	public Player(String name) {
		this(name, false, false);
	}
	
	public void addWin() {
		this.wins++;
	}
	
	public int getWins() {
		return wins;
	}
	
	public void addLoss() {
		this.loses++;
	}
	
	public int getLoses() {
		return loses;
	}
	
	public String getName() {
		return name;
	}
	
	public boolean isNPC() {
		return npc;
	}
	
	public boolean isDealer() {
		return dealer;
	}
	
	public boolean isBusted() {
		return busted;
	}
	
	public boolean addCard(Card card) {
		return this.hand.add(card);
	}
	
	public Collection<Card> getHand() {
		return hand;
	}
}