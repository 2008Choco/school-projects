package me.choco.blackjack;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

import me.choco.blackjack.cards.Card;
import me.choco.blackjack.cards.CardSuit;
import me.choco.blackjack.cards.CardType;
import me.choco.blackjack.game.Player;
import me.choco.blackjack.game.utils.PlayerHandler;

public class Blackjack {
	
	private static final String[] POSSIBLE_PLAYER_NAMES = {"Johnathan", "Gilbert", "Tyson", "Pat", "Madison", "Chance", "Junior", "Ava", "Forest", "Roselyn"};
	private static final int MAX_PLAYERS = 4, CONTROLLED_PLAYERS = 1;
	
	private static final Random random = new Random();
	private final Scanner scanner = new Scanner(System.in);
	
	private final PlayerHandler playerHandler = new PlayerHandler();
	private final List<Card> deck = new ArrayList<>();
	
	public Blackjack() {
		// Generate cards
		for (CardSuit suit : CardSuit.values()){
			for (CardType type : CardType.values()){
				this.deck.add(new Card(suit, type));
			}
		}
		Collections.shuffle(deck);
		
		System.out.print("What is your name: ");
		String playerName = scanner.nextLine();
		System.out.println();
		
		this.playerHandler.addPlayer(new Player(playerName));
		
		for (int i = 0; i < MAX_PLAYERS - CONTROLLED_PLAYERS; i++) {
			String name;
			while (this.playerHandler.getPlayer((name = POSSIBLE_PLAYER_NAMES[random.nextInt(POSSIBLE_PLAYER_NAMES.length)])) != null) {}
			
			boolean dealer = random.nextBoolean();
			if (dealer && this.playerHandler.hasDealerBeenSet()) 
				dealer = false;
			if (i == MAX_PLAYERS - CONTROLLED_PLAYERS - 1 && !this.playerHandler.hasDealerBeenSet()) 
				dealer = true;
			
			Player player = new Player(name, true, dealer);
			this.playerHandler.addPlayer(player);
			if (dealer)
				System.out.println(player.getName() + " has been selected as the dealer!");
		}
		
		System.out.println("Players:");
		for (Player player : this.playerHandler.getPlayers())
			System.out.println(" - " + player.getName());
		
		// TODO: GAME LOGIC
		
		scanner.close();
	}
	
	public static void main(String[] args) { new Blackjack(); }
}