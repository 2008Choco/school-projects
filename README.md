# README #

This is a readme file. If you are viewing this file, you are more than likely on the overview page of the repository on the BitBucket website. Below, you will find information on what this repository is for, terms of use, and requirements to download the source

### What is this repository for? ###

This repository is to hold on school projects created by Parker "2008Choco" Hawke for school purposes

* Class Code: TEJ3E4E
* Class Title: Computer Hardware / Programming
* Teacher: K. McDaniel
* School: St. Stephen Secondary School
* Current projects include: /BinaryDecimalConverter/, /Ohm's Law Converter/

### How do I get set up? ###

* Download Atlassian SourceTree (Make sure you have an Atlassian account as well)
* Be sure to have Eclipse installed on your computer
* Grab the Clone URL for the repository (https://2008Choco@bitbucket.org/2008Choco/school-projects.git)
* Go in to your SourceTree program, and click on the button that says "Clone New Repository"
* Create an empty folder somewhere you'll remember to store your repository
* The repository is now on your computer. To get it in to Eclipse, File -> Import -> Projects From Git -> Existing Repository
* Click "add", then "browse", and find the file you saved your repository to
* After finding the .git repository, double click on it. You will see a list of the folders (projects) in the repository
* Double click on whatever project you would like to import in to Eclipse (for example, Ohm's Law)
* Finally, click on the checkbox next to the project file, and hit Finish. Congratulations, you now have the Ohm's Law project in Eclipse to edit!

### Contribution guidelines ###

This code is open source, but there should be no modification of code unless you have explicit permission from myself, Parker "2008Choco" Hawke. All software and projects in this repository are copywriten under the name of Parker Hawke year 2015. All Rights Reserved Licensing. You do not have any right to modify, copy, delete, or redistribute, in any or all forms of binary or .java class files. Any modification or copying of code in this repository WITHOUT a fork, will be held against you for Copyright purposes.

Copyright 2015 - Parker Hawke - All Rights Reserved

### Who do I talk to? ###

You may get in contact with Parker Hawke, whether it is through e-mail (hawkeboyz2@hotmail.com), or through a fork request over the BitBucket repo.

### Notes ###

This repository is all for school purposes. All code is for educational purposes and you are free to look at the code to educate yourself on how to use it. Note that not all of the code in this repository will be the most efficient, nor the best way to use this code in the slightest. Damage to any property on your computer or software may not, and will not, be held against me. Use this source code at your own risk, please and thank you