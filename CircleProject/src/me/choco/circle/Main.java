package me.choco.circle;

import java.text.DecimalFormat;

public class Main {
	public static void main(String[] args) {
		//This is our first constructor we created
		Circle circle = new Circle(15);
		
		//This is our second constructor, with a name parameter
		Circle circle2 = new Circle(27, "Circle 2");
		
		//Print out the string variations of our circles
		System.out.println("Our Circles: ");
		System.out.println(circle); 
		System.out.println(circle2);
		System.out.println();
		
		//Get a chord from one of our circles (Half of the given radius, that being 7.5)
		System.out.println("Chord Length for Circle #1: ");
		double circleChordLength1 = circle.getChordLength(circle.getRadius() / 2);
		System.out.println(circleChordLength1);
		System.out.println();
		
		/* REMEMBER: These will throw an IllegalArgumentExeption
		 * circle.getChordLength(-1)
		 * circle.getChordLength(circle.getRadius() + 1)
		 
		 * Reasons: A chord cannot be -1 away from the center
		 *          The chord cannot be outside of the circle
		 */
		System.out.println("Initial Area: " + circle.getArea());
		System.out.println(circle.getArea(circle.getSectorArea(40), circle.getSectorArea(50)));
		System.out.println(circle.getArea(circle.getSectorArea(180)));
		System.out.println();
		
		/* REMEMBER: These will throw an IllegalArgumentException
		 * circle.getSectorArea(-1)
		 * circle.getSectorArea(361)
		 * circle.getArea(circle.getSectorArea(180), circle.getSectorArea(360))
		 * 
		 * Reasons: A sector angle cannot be negative
		 *          An angle cannot be greater than 360
		 *          You cannot remove an area greater than an existing area
		 */
		
		//Parameters to round the products are available. 
		//Some require the DecimalFormat BEFORE all other parameters, some require it after
		DecimalFormat format = new DecimalFormat("0.00");
		System.out.println("Rounding our Areas:");
		System.out.println("Initial Area (Rounded): " + circle.getArea(format));
		System.out.println(circle.getArea(format, circle.getSectorArea(40), circle.getSectorArea(50)));
		System.out.println(circle.getArea(format, circle.getSectorArea(180)));
	}
}