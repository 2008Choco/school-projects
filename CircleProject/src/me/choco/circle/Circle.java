package me.choco.circle;

import java.text.DecimalFormat;

public class Circle extends Object{
	
	//This declares global variables that the circle needs to function
	protected String name;
	protected double radius = 0;
	
	//Constructor 1: No name parameter. Thus, "private String name" will be null
	public Circle(double radius){
		this.radius = radius;
	}
	
	//Constructor 2: Name parameter. The "private String name" will now be initialized
	public Circle(double radius, String name){
		this(radius);
		this.name = name;
	}
	
	/** Get the radius of the circle
	 * @return double - The radius
	 */
	public double getRadius() {
		return radius;
	}

	/** Get the diameter of the circle. (Radius * 2)
	 * @return double - The diameter
	 */
	public double getDiameter() {
		return radius * 2;
	}

	/** Get the circumference of the circle. (diameter * Pi)
	 * @return double - The circumference
	 */
	public double getCircumference() {
		return getDiameter() * Math.PI;
	}
	
	/** Get the circumference of the circle. (diameter * Pi)
	 * @param DecimalFormat - The format to round the number
	 * @return double - The circumference
	 */
	public double getCircumference(DecimalFormat format) {
		return Double.parseDouble(format.format(getCircumference()));
	}

	/** Get the area of the circle. (Pi * Radius^2)
	 * @return double - The area
	 */
	public double getArea() {
		return Math.PI * Math.pow(radius, 2);
	}
	
	/** Get the area of the circle. (Pi * Radius^2)
	 * @param DecimalFormat - The format to round the number
	 * @return double - The area
	 */
	public double getArea(DecimalFormat format) {
		return Double.parseDouble(format.format(getArea()));
	}
	
	/** Get the area, subtracted by the specified area
	 * @param areaToRemove - The amount of area to remove from the circle (getSectorArea(double) may be used)
	 * @return double - The area
	 */
	public double getArea(double... areaToRemove){
		double removalArea = 0;
		for (double area : areaToRemove)
			removalArea += area;
		
		if (removalArea > getArea()) 
			throw new IllegalArgumentException("You cannot remove more area than the circle's area (" + removalArea + "/" + getArea() + ")");
		
		return getArea() - removalArea;
	}
	
	/** Get the area, subtracted by the specified area
	 * @param DecimalFormat - The format to round the number
	 * @param areaToRemove - The amount of area to remove from the circle (getSectorArea(double) may be used)
	 * @return double - The area
	 */
	public double getArea(DecimalFormat format, double... areaToRemove){
		return Double.parseDouble(format.format(getArea(areaToRemove)));
	}
	
	/** Get the area of a sector within the circle between an angle from one line to another
	 * @param angle - The angle between two lines drawn from the center to the outside of the circle
	 * @return double - The area of the sector
	 */
	public double getSectorArea(double angle){
		if (angle > 360) throw new IllegalArgumentException("Angles cannot be greater than 360");
		if (angle < 0) throw new IllegalArgumentException("Angles cannot be negative");
		
		return (angle/360)*getArea();
	}
	
	/** Get the area of a sector within the circle between an angle from one line to another
	 * @param angle - The angle between two lines drawn from the center to the outside of the circle
	 * @param DecimalFormat - The format to round the number
	 * @return double - The area of the sector
	 */
	public double getSectorArea(double angle, DecimalFormat format){
		return Double.parseDouble(format.format(getSectorArea(angle)));
	}
	
	/** Get the length of a chord, relative to the distance from the center of the circle
	 * @param distanceFromCenter - The distance from the center of the circle to create a chord
	 * @return double - The length of the created chord
	 */
	public double getChordLength(double distanceFromCenter){
		if (distanceFromCenter > getRadius()) throw new IllegalArgumentException("Distance from center cannot be larger than the radius");
		if (distanceFromCenter < 0) throw new IllegalArgumentException("Distance from center cannot be negative");
		
		return 2 * (Math.sqrt(Math.pow(getRadius(), 2) - Math.pow(distanceFromCenter, 2)));
	}
	
	/** Get the length of a chord, relative to the distance from the center of the circle
	 * @param distanceFromCenter - The distance from the center of the circle to create a chord
	 * @param DecimalFormat - The format to round the number
	 * @return double - The length of the created chord
	 */
	public double getChordLength(double distanceFromCenter, DecimalFormat format){
		return Double.parseDouble(format.format(getChordLength(distanceFromCenter)));
	}
	
	/** Get the name of the circle (if provided)
	 * @return String - The name
	 */
	public String getName(){
		return name;
	}

	//When called, returns a String variation of the circle for English readability
	@Override
	public String toString(){
		return "Circle:{"
				+ "name: " + getName() 
				+ ", radius: " + getRadius() 
				+ ", diameter: " + getDiameter() 
				+ ", circumference: " + getCircumference() 
				+ ", area: " + getArea() + "}";
	}
	
	//Overriding .equals() to determine if the circles are the exact same (relative to radius)
	//If the radius is the same, obviously all variables using radius/diameter will be the same
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Circle)
			return (((Circle) obj).getRadius() == this.getRadius());
		return false;
	}
}