package me.choco.circle;

import java.awt.Color;

public class GraphicCircle extends Circle{

	private Color color;
	public GraphicCircle(double radius){
		super(radius);
	}
	
	public GraphicCircle(double radius, String name){
		super(radius, name);
		this.name = name;
	}
	
	public GraphicCircle(double radius, Color color){
		this(radius);
		this.color = color;
	}
	
	public GraphicCircle(double radius, String name, Color color){
		this(radius, color);
		this.name = name;
	}
	
	public Color getColor(){
		return color;
	}
	
	public void setColor(Color color){
		this.color = color;
	}

	@Override
	public String toString(){
		return "Circle:{"
				+ "name: " + getName() 
				+ ", radius: " + getRadius() 
				+ ", diameter: " + getDiameter() 
				+ ", circumference: " + getCircumference() 
				+ ", area: " + getArea() + "}";
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Circle)
			return (((Circle) obj).getRadius() == this.getRadius());
		return false;
	}
}