package me.choco.temperature;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JFrame;

import me.choco.temperature.utils.ConversionPanel;

public class TempConverter {
	
	public static void main(String[] args) {
		BufferedImage image = null;
		try {image = ImageIO.read(new File("Resources/Thermometer.jpg"));}catch (IOException e) {}
		
		JFrame frame = new JFrame("Temperature Converter");
		frame.setEnabled(true);
		frame.setIconImage(image);
		frame.setSize(325, 150);
		frame.setResizable(false);
		frame.setLocationRelativeTo(null);
		frame.add(new ConversionPanel());
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
	}
}