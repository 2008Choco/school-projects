package me.choco.temperature.utils;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

@SuppressWarnings("serial")
public class ConversionPanel extends JPanel{
	
	public JTextField celciusField = new JTextField(); public JLabel binaryLabel = new JLabel("Celcius: ");
	public JTextField fahrenheitField = new JTextField(); public JLabel decimalLabel = new JLabel("Fahrenheit: ");
	public JButton convertButton = new JButton("Convert");
	
	private TemperatureMath math = new TemperatureMath();
	
	public ConversionPanel(){
		setLayout(new GridBagLayout());
		GridBagConstraints constraints = new GridBagConstraints();
		constraints.insets = new Insets(5, 5, 5, 5);
		constraints.anchor = GridBagConstraints.CENTER;
		constraints.fill = GridBagConstraints.BOTH;
		
		constraints.gridx = 0;
		constraints.gridy = 0;
		add(binaryLabel, constraints);
		
		constraints.gridx = 1;
		constraints.weightx = 0.5;
		add(celciusField, constraints);
		
		constraints.gridx = 1;
		constraints.gridy = 1;
		constraints.weightx = 0.0;
		add(convertButton, constraints);
		
		constraints.gridx = 0;
		constraints.gridy = 2;
		add(decimalLabel, constraints);
		
		constraints.gridx = 1;
		constraints.weightx = 0.5;
		add(fahrenheitField, constraints);
		
		convertButton.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent event){
				if (!celciusField.getText().equals("") && fahrenheitField.getText().equals("")){
					fahrenheitField.setText(math.celciusToFahrenheit(celciusField.getText()));
				}else if (!fahrenheitField.getText().equals("") && celciusField.getText().equals("")){
					celciusField.setText(math.fahrenheitToCelcius(fahrenheitField.getText()));
				}else{
					fahrenheitField.setText(math.celciusToFahrenheit(celciusField.getText()));
				}
			}
		});
	}
}