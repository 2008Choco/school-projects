package me.choco.temperature.utils;

import java.text.DecimalFormat;

public class TemperatureMath {
	
	DecimalFormat df = new DecimalFormat("#.#");
	
	public String celciusToFahrenheit(String input){
		try{
			double celcius = Integer.parseInt(input);
			return df.format((celcius * 1.8) + 32);
		}catch(NumberFormatException e){return "0.0";}
	}
	
	public String fahrenheitToCelcius(String input){
		try{
			double fahrenheit = Integer.parseInt(input);
			return String.valueOf(Math.round((fahrenheit - 32) / 1.8));
		}catch(NumberFormatException e){return "0.0";}
	}
}