package me.choco.ohmslaw.utils;

public class OhmsMath {
	public double performOhmsLawMath(String voltsInput, String currentInput, String resistanceInput){
    	double volts = tryParse(voltsInput);
    	double current = tryParse(currentInput);
    	double resistance = tryParse(resistanceInput);
    	
    	if (volts == 0){return current * resistance;}
    	else if (current == 0){return volts / resistance;}
    	else if (resistance == 0){return volts / current;}
    	else {return 0;}
	}
	
	public String generateAnswerAsEquationString(double result, String voltsInput, String currentInput, String resistanceInput){
		double volts = tryParse(voltsInput);
    	double current = tryParse(currentInput);
    	double resistance = tryParse(resistanceInput);
    	String answer = "";
    	
		if (volts == 0){answer = result + "V = " + current + " x " + resistance;}
		else if (current == 0){answer = result + "amperes = " + volts + " ÷ " + resistance;}
		else if (resistance == 0){answer = result + "Ω = " + volts + " ÷ " + current;}
		return answer;
	}
	
	private double tryParse(String number){
		try{
			return Double.parseDouble(number);
		}catch(NumberFormatException e){
			return 0;
		}
	}
}