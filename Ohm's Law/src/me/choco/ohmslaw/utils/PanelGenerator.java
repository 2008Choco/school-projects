package me.choco.ohmslaw.utils;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

@SuppressWarnings("serial")
public class PanelGenerator extends JPanel{
	JFrame frame;
	public PanelGenerator(JFrame frame){
		this.frame = frame;
	}
	
	OhmsMath math = new OhmsMath();

	JLabel labelVolts = new JLabel("Voltage: ");
	JLabel labelCurrent = new JLabel("Current: ");
	JLabel labelResistance = new JLabel("Resistance: ");
	JLabel answerLabel = new JLabel("");
	JTextField textVolts = new JTextField(20);
	JTextField textCurrent = new JTextField(20);
	JTextField textResistance = new JTextField(20);
	JButton confirmButton = new JButton("Calculate");
	
	public JPanel createCalculationPanel() {
		this.setLayout(new GridBagLayout());
		
		GridBagConstraints constraints = new GridBagConstraints();
		constraints.anchor = GridBagConstraints.WEST;
		constraints.insets = new Insets(5, 5, 5, 5);
		
		constraints.gridx = 0;
		constraints.gridy = 0;
		add(labelVolts, constraints);

		constraints.gridx = 1;
		add(textVolts, constraints);

		constraints.gridx = 0;
		constraints.gridy = 1;
		add(labelCurrent, constraints);

		constraints.gridx = 1;
		add(textCurrent, constraints);

		constraints.gridx = 0;
		constraints.gridy = 2;
		add(labelResistance, constraints);

		constraints.gridx = 1;
		add(textResistance, constraints);

		constraints.gridx = 2;
		constraints.gridy = 1;
		add(confirmButton, constraints);
		
		constraints.gridx = 0;
		constraints.gridy = 3;
		constraints.gridwidth = 3;
		add(answerLabel, constraints);
		
	    confirmButton.addActionListener(new ActionListener() {
	        public void actionPerformed(ActionEvent e) {
	        	String voltsInput = textVolts.getText(), currentInput = textCurrent.getText(), resistanceInput = textResistance.getText();
	        	double result = math.performOhmsLawMath(voltsInput, currentInput, resistanceInput);
	        	answerLabel.setText(math.generateAnswerAsEquationString(result, voltsInput, currentInput, resistanceInput));
	        	repaint();
	        }
	    });

		setBorder(BorderFactory.createLineBorder(Color.darkGray));
		return this;
	}
}