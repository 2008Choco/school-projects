package me.choco.ohmslaw;

import javax.swing.JFrame;
import javax.swing.UIManager;

import me.choco.ohmslaw.utils.PanelGenerator;

/* Author: Parker "2008Choco" Hawke
 * 
 * This program was created for educational purposes. You are hereby prohibited to use any
 *          of this source code, unless specified otherwise by Parker Hawke.
 * 
 *  This code is open source for viewing purposes ONLY! You are not to redistribute this code
 * for any monetary value. Any non-monetary value trades is welcomed as long as I, Parker Hawke,
 *  have received credit for the work of this code. You are not to claim this code as your own.
 * 
 *                          All Rights Reserved - Parker Hawke - 09/17/2015
 */

/** This is the Main JFrame method to open the Ohm's Law calculation input
 * The Main JFrame Class will run in the main method FIRST
 * @author 2008Choco - Parker Hawke
 * @since 9/17/2015, September 17th, 2015
 */
public class Main{
	public static void main(String args[]){
		JFrame frame = new JFrame("Ohm's Law Converter");
		PanelGenerator generator = new PanelGenerator(frame);

		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		
		frame.setVisible(true);
		frame.setLocationRelativeTo(null);
		frame.add(generator.createCalculationPanel());
		frame.setSize(430, 170);
		frame.setResizable(false);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
}